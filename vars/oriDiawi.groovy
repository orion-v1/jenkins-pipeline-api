
def upload(Map p)
{
    if (p.credentialsId == null)
    {
        error("Missing parameter credentialsId")
        return
    }

    if (p.file == null)
    {
        error("Missing parameter file")
        return
    }

    def result = [:]

    withCredentials([string(credentialsId: p.credentialsId, variable: 'TOKEN')])
    {
        // upload file
        def json = callCurl("https://upload.diawi.com/ -F token=\$TOKEN -F file=@${p.file} -F find_by_udid=1 -H \"Cache-Control: no-cache\"", 3, 3000)
        if (json == null || json.job == null)
        {
            // TODO: failed to upload
            echo "Failed to upload to Diawi"
            result.success = false
            return result
        }

        // get link
        def jobId = json.job
        waitUntil(initialRecurrencePeriod: 2000) // quiet: true
        {
            json = callCurl("https://upload.diawi.com/status?token=\$TOKEN\\&job=${jobId}", -1, 2000)
            if (json == null)
                return false;

            if (json.status == 2001) // not yet ready
                return false

            result = json
            result.success = json.status == 2000
            return true            
        }
    }

    return result
}

def callCurl(String query, int attemps, int recurrencePeriod)
{
    def result 

    waitUntil(initialRecurrencePeriod: recurrencePeriod)
    {
        try
        {
            def json = sh(script: "curl --http1.1 ${query}", returnStdout: true)
            result = readJSON(text: json)
            return true
        }
        catch (e)
        {
            echo e.toString()
        }

        // infinite execution
        if (attemps < 0)
            return false
        
        // attemps limited execution
        attemps--
        if (attemps <= 0)
            return true

        return false
    }

    return result
}
