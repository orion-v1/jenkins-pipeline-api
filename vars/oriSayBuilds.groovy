
def upload(Map p)
{
    if (p.credentialsId == null)
    {
        error("Missing parameter credentialsId")
        return
    }

    if (p.file == null)
    {
        error("Missing parameter file")
        return
    }

    def result = [:]

    withCredentials([string(credentialsId: p.credentialsId, variable: 'TOKEN')])
    {
        // upload file
        def json = callCurl("https://builds.saygames.io/api/upload -F token=\$TOKEN -F file=@${p.file}", 3, 3000)
        if (json == null)
        {
            result.success = false
            result.message = "No response from server."
            echo "Failed to upload to SayBuilds."
            return result
        }

        if (json.url == null)
        {
            result.success = false
            result.message = json.error
            echo "Failed to upload to SayBuilds."
            return result
        }

        result.success = true
        result.url = json.url
        return result
    }
}

def callCurl(String query, int attemps, int recurrencePeriod)
{
    def result 

    waitUntil(initialRecurrencePeriod: recurrencePeriod)
    {
        try
        {
            def json = sh(script: "curl ${query}", returnStdout: true)
            result = readJSON(text: json)
            return true
        }
        catch (e)
        {
            echo e.toString()
        }

        // infinite execution
        if (attemps < 0)
            return false
        
        // attemps limited execution
        attemps--
        if (attemps <= 0)
            return true

        return false
    }

    return result
}
