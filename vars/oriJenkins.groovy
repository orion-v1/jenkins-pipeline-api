
def setCurrentBuildResultWithException(def exception)
{
    if (exception instanceof org.jenkinsci.plugins.workflow.steps.FlowInterruptedException)
        currentBuild.result = 'ABORTED'
    else
        currentBuild.result = 'FAILURE'

    //echo "GROOVY EXCEPTION: ${e}"
}

def isBuildAborted()
{
    return currentBuild.currentResult == 'ABORTED'
}

def isBuildSucceeded()
{
    return currentBuild.currentResult == 'SUCCESS'
}

def isBuildUnstable()
{
    return currentBuild.currentResult == 'UNSTABLE'
}

def isBuildFailed()
{
    return currentBuild.currentResult == 'FAILURE'
}
