def createWithFileContent(Map p)
{
    def exists = fileExists p.file
    if (!exists) return
    
    def text = p.text
    if (p.isMarkedList)
    {
        text += "<ul>"
    }
    else if (text != null) text += "<br>"

    def summary = addSummary(icon: p.icon, id: p.id, text: text)

    def data = readFile(file: p.file)
    data.readLines().each {
        if (p.isMarkedList) summary.appendText("<li>$it</li>")
        else summary.appendText("$it<br>")
    }

    if (p.isMarkedList)
    {
        text += "</ul>"
    }
}
