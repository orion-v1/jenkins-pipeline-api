
def unlockKeyChain(Map p)
{
    if (p.credentialsId == null)
    {
        error("Missing parameter credentialsId")
        return
    }

    withCredentials([usernamePassword(
        credentialsId: p.credentialsId,
        usernameVariable: 'USERNAME',
        passwordVariable: 'PASSWORD')])
    {
        if (p.keychainPath == null)
        {
            p.keychainPath = "~/Library/Keychains/\$USERNAME.keychain-db"
        }

        sh "security unlock-keychain -p \$PASSWORD ${p.keychainPath}"

        if (p.lockAfterTimeout != null)
        { 
            int time = p.lockAfterTimeout
            if (time < 0)
            {
                // no timeout
                sh "security set-keychain-settings -u ${p.keychainPath}"
            }
            else
            {
                // timeout in seconds
                sh "security set-keychain-settings -u -t ${time} ${p.keychainPath}"
            }
        }
    }
}
