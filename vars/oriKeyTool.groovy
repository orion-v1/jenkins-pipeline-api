
def generateKeyStore(Map p)
{
    if (p.credentialsId == null)
    {
        error("Missing parameter credentialsId")
        return
    }

    if (p.keystorePath == null)
    {
        error("Missing parameter keystorePath")
        return
    }

    if (p.keyAlias == null)
    {
        error("Missing parameter keyAlias")
        return
    }

    if (p.ownerName == null)
    {
        error("Missing parameter ownerName")
        return
    }

    // PKCS#12 keystore password == key password
    withCredentials([string(
        credentialsId: p.credentialsId,
        variable: 'KEYPASSWD')])
    {
        sh "keytool -genkeypair -v -storetype pkcs12 -dname \"${p.ownerName}\" -keystore \"${p.keystorePath}\" -storepass \${KEYPASSWD} -alias ${p.keyAlias} -keypass \${KEYPASSWD} -keyalg RSA -keysize 2048 -validity 10000"
    }
}
