package io.orion.jenkins.unity.versioning

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class UnityVersionTests 
{
    @Test
    void testYearConstructor() 
    {
        UnityVersion version = new UnityVersion(2020)
        Assertions.assertEquals("2020", version.toString())
    }

    @Test
    void testYearMajorConstructor() 
    {
        UnityVersion version = new UnityVersion(2020, 3)
        Assertions.assertEquals("2020.3", version.toString())
    }

    @Test
    void testYearMajorMinorConstructor() 
    {
        UnityVersion version = new UnityVersion(2020, 3, 42)
        Assertions.assertEquals("2020.3.42", version.toString())
    }

    @Test
    void testYearMajorMinorTypeConstructor() 
    {
        UnityVersion version = new UnityVersion(2020, 3, 42, 'f')
        Assertions.assertEquals("2020.3.42f", version.toString())
    }

    @Test
    void testYearMajorMinorTypeRevisionConstructor() 
    {
        UnityVersion version = new UnityVersion(2020, 3, 42, 'f', 1)
        Assertions.assertEquals("2020.3.42f1", version.toString())
    }

    @Test
    void testInvalidTypeConstructor()
    {
        Assertions.assertThrows(Exception.class, { new UnityVersion(2020, 3, 42, 'abc', 1) })
    }



    @Test
    void testToString()
    {
        Assertions.assertEquals("2020", new UnityVersion(2020).toString())
        Assertions.assertEquals("2020.3", new UnityVersion(2020, 3).toString())
        Assertions.assertEquals("2020.3.42", new UnityVersion(2020, 3, 42).toString())
        Assertions.assertEquals("2020.3.42f", new UnityVersion(2020, 3, 42, 'f').toString())
        Assertions.assertEquals("2020.3.42f1", new UnityVersion(2020, 3, 42, 'f', 1).toString())
    }



    @Test
    void testYearStringConstructor()
    {
        UnityVersion version = new UnityVersion("2020")
        Assertions.assertEquals("2020", version.toString())
    }

    @Test
    void testYearMajorStringConstructor()
    {
        UnityVersion version = new UnityVersion("2020.3")
        Assertions.assertEquals("2020.3", version.toString())
    }

    @Test
    void testYearMajorMinorStringConstructor()
    {
        UnityVersion version = new UnityVersion("2020.3.42")
        Assertions.assertEquals("2020.3.42", version.toString())
    }

    @Test
    void testYearMajorMinorTypeStringConstructor()
    {
        UnityVersion version = new UnityVersion("2020.3.42f")
        Assertions.assertEquals("2020.3.42f", version.toString())
    }

    @Test
    void testYearMajorMinorTypeRevisionStringConstructor()
    {
        UnityVersion version = new UnityVersion("2020.3.42f1")
        Assertions.assertEquals("2020.3.42f1", version.toString())
    }

    @Test
    void testInvalidTypeStringConstructor()
    {
        Assertions.assertThrows(Exception.class, { new UnityVersion("2020.3.42abc1") })
    }

    @Test
    void testSkippedMajorStringConstructor()
    {
        Assertions.assertThrows(Exception.class, { new UnityVersion("2020..42f1") })
    }

    @Test
    void testSkippedYearStringConstructor()
    {
        Assertions.assertThrows(Exception.class, { new UnityVersion(".3.42f1") })
    }

    @Test
    void testSkippedYearMajorStringConstructor()
    {
        Assertions.assertThrows(Exception.class, { new UnityVersion("42f1") })
    }



    @Test
    void testYearGetter()
    {
        UnityVersion version = new UnityVersion("2020.3.42f1")
        Assertions.assertEquals(2020, version.year)
    }

    @Test
    void testMajorGetter()
    {
        UnityVersion version = new UnityVersion("2020.3.42f1")
        Assertions.assertEquals(3, version.major)
    }

    @Test
    void testMinorGetter()
    {
        UnityVersion version = new UnityVersion("2020.3.42f1")
        Assertions.assertEquals(42, version.minor)
    }

    @Test
    void testTypeGetter()
    {
        UnityVersion version = new UnityVersion("2020.3.42f1")
        Assertions.assertEquals('f' as char, version.type)
    }

    @Test
    void testRevisionGetter()
    {
        UnityVersion version = new UnityVersion("2020.3.42f1")
        Assertions.assertEquals(1, version.revision)
    }



    @Test
    void testEqualComparison()
    {
        Assertions.assertEquals(true, new UnityVersion("2020") == new UnityVersion("2020"))
        Assertions.assertEquals(false, new UnityVersion("2020") == new UnityVersion("2021"))
        Assertions.assertEquals(true, new UnityVersion("2020.3.42") == new UnityVersion("2020.3.42"))
        Assertions.assertEquals(false, new UnityVersion("2020.3.42") == new UnityVersion("2020.3.42f1"))
    }

    @Test
    void testGreaterComparison()
    {
        Assertions.assertEquals(true, new UnityVersion("2021") > new UnityVersion("2020"))
        Assertions.assertEquals(false, new UnityVersion("2020") > new UnityVersion("2020"))
        Assertions.assertEquals(true, new UnityVersion("2020.3.42f1") > new UnityVersion("2020.3.42"))
        Assertions.assertEquals(false, new UnityVersion("2020.3.42") > new UnityVersion("2020.3.42"))
        Assertions.assertEquals(false, new UnityVersion("2020.2.42") > new UnityVersion("2020.3.42"))
        Assertions.assertEquals(true, new UnityVersion("2020.3.42f1") > new UnityVersion("2020"))
    }

    @Test
    void testGreaterOrEqualComparison()
    {
        Assertions.assertEquals(true, new UnityVersion("2021") >= new UnityVersion("2020"))
        Assertions.assertEquals(true, new UnityVersion("2020") >= new UnityVersion("2020"))
        Assertions.assertEquals(true, new UnityVersion("2020.3.42f1") >= new UnityVersion("2020.3.42"))
        Assertions.assertEquals(true, new UnityVersion("2020.3.42") >= new UnityVersion("2020.3.42"))
        Assertions.assertEquals(false, new UnityVersion("2020.2.42") >= new UnityVersion("2020.3.42"))
        Assertions.assertEquals(true, new UnityVersion("2020.3.42f1") >= new UnityVersion("2020"))
    }

    @Test
    void testLessComparison()
    {
        Assertions.assertEquals(true, new UnityVersion("2020") < new UnityVersion("2021"))
        Assertions.assertEquals(false, new UnityVersion("2020") < new UnityVersion("2020"))
        Assertions.assertEquals(true, new UnityVersion("2020.3.42") < new UnityVersion("2020.3.42f1"))
        Assertions.assertEquals(false, new UnityVersion("2020.3.42") < new UnityVersion("2020.3.42"))
        Assertions.assertEquals(false, new UnityVersion("2020.3.42") < new UnityVersion("2020.2.42"))
        Assertions.assertEquals(true, new UnityVersion("2020") < new UnityVersion("2020.3.42f1"))
    }

    @Test
    void testLessOrEqualComparison()
    {
        Assertions.assertEquals(true, new UnityVersion("2020") <= new UnityVersion("2021"))
        Assertions.assertEquals(true, new UnityVersion("2020") <= new UnityVersion("2020"))
        Assertions.assertEquals(true, new UnityVersion("2020.3.42") <= new UnityVersion("2020.3.42f1"))
        Assertions.assertEquals(true, new UnityVersion("2020.3.42") <= new UnityVersion("2020.3.42"))
        Assertions.assertEquals(false, new UnityVersion("2020.3.42") <= new UnityVersion("2020.2.42"))
        Assertions.assertEquals(true, new UnityVersion("2020") <= new UnityVersion("2020.3.42f1"))
    }



    @Test
    void testEquality()
    {
        Assertions.assertEquals(true, new UnityVersion("2020").equals(new UnityVersion("2020")))
        Assertions.assertEquals(false, new UnityVersion("2020").equals(new UnityVersion("2021")))
        Assertions.assertEquals(true, new UnityVersion("2020.3.42").equals(new UnityVersion("2020.3.42")))
        Assertions.assertEquals(false, new UnityVersion("2020.3.42").equals(new UnityVersion("2020.3.42f1")))
    }
}
