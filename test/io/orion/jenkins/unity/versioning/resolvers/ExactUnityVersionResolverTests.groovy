package io.orion.jenkins.unity.versioning.resolvers

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import io.orion.jenkins.unity.versioning.UnityVersion

class ExactUnityVersionResolverTests
{
    private List<UnityVersion> availableVersions = [
        new UnityVersion("2019.3.9f1"),
        new UnityVersion("2019.4.38f1"),
        new UnityVersion("2020.2.13f1"),
        new UnityVersion("2020.3.21f1"),
        new UnityVersion("2020.3.42f1"), 
        new UnityVersion("2021.2.8f1"), 
        new UnityVersion("2021.3.14f1"), 
        new UnityVersion("2022.1.23f1"), 
        new UnityVersion("2023.1.0a21")
    ]



    @Test
    void testFullySpecifiedAvailableVersion()
    {
        def resolver = new ExactUnityVersionResolver()
        def preferredVersion = new UnityVersion("2020.3.42f1")
        Assertions.assertEquals(new UnityVersion("2020.3.42f1"), resolver.resolve(preferredVersion, availableVersions))
    }

    @Test
    void testMajorAvailableVersion()
    {
        def resolver = new ExactUnityVersionResolver()
        def preferredVersion = new UnityVersion("2020.3")
        Assertions.assertEquals(new UnityVersion("2020.3.42f1"), resolver.resolve(preferredVersion, availableVersions))
    }

    @Test
    void testYearAvailableVersion()
    {
        def resolver = new ExactUnityVersionResolver()
        def preferredVersion = new UnityVersion("2020")
        Assertions.assertEquals(new UnityVersion("2020.3.42f1"), resolver.resolve(preferredVersion, availableVersions))
    }

    @Test
    void testFullySpecifiedUnavailableVersion()
    {
        def resolver = new ExactUnityVersionResolver()
        def preferredVersion = new UnityVersion("2020.3.30f1")
        Assertions.assertEquals(null, resolver.resolve(preferredVersion, availableVersions))
    }

    @Test
    void testMajorUnavailableVersion()
    {
        def resolver = new ExactUnityVersionResolver()
        def preferredVersion = new UnityVersion("2022.3")
        Assertions.assertEquals(null, resolver.resolve(preferredVersion, availableVersions))
    }

    @Test
    void testYearUnavailableVersion()
    {
        def resolver = new ExactUnityVersionResolver()
        def preferredVersion = new UnityVersion("2024")
        Assertions.assertEquals(null, resolver.resolve(preferredVersion, availableVersions))
    }
}
