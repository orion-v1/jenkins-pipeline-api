package io.orion.jenkins

import io.orion.jenkins.Path

public class WorkspaceSnapshot implements Serializable
{
    private IJenkinsBridge _jenkins
    private String _name

    public WorkspaceSnapshot(IJenkinsBridge jenkins, String name)
    {
        _jenkins = jenkins
        _name = name
    }

    private String getWorkspacePath()
    {
        return _jenkins.getWorkspacePath()
    }

    private String getSnapshotPath()
    {
        return Path.combine(_jenkins.getWorkspaceTempPath(), _name)
    }

    public void addPath(String path)
    {
        String workspacePath = getWorkspacePath();
        String snapshotPath = getSnapshotPath();

        _jenkins.sh """
            #!/bin/bash

            set -e
            mkdir -p "$snapshotPath"
            set +e

            PARENT_DIR=\$(dirname "${path}")
            if [ "\$PARENT_DIR" = "." ]; then
                PARENT_DIR=""
            fi

            if [[ -d "$snapshotPath/$path" || -f "$snapshotPath/$path" ]]; then
                rsync --remove-source-files -aW "$workspacePath/${path}" "$snapshotPath/\$PARENT_DIR/"
            else
                mkdir -p "$snapshotPath/\$PARENT_DIR"
                mv -f "$workspacePath/${path}" "$snapshotPath/\$PARENT_DIR/"
            fi

            if [ \$? -eq 0 ]; then 
                echo "Added to snapshot: $workspacePath/${path}"
            else
                echo "FAILED to add to snapshot: $workspacePath/${path}"
            fi

            exit 0
        """
    }

    public void restore()
    {
        String workspacePath = getWorkspacePath();
        String snapshotPath = getSnapshotPath();

        _jenkins.sh """
            #!/bin/bash
            set +e
            rsync --remove-source-files -aW "$snapshotPath/" "$workspacePath/"
            rm -fR "$snapshotPath"
            exit 0
        """
    }
}
