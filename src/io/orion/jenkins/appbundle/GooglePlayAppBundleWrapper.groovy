package io.orion.jenkins.appbundle

import io.orion.jenkins.IJenkinsBridge
import io.orion.jenkins.ShellCommandBuilder

public class GooglePlayAppBundleWrapper implements Serializable
{
    private IJenkinsBridge _jenkins

    // DEPRECATED! Used for support legacy code
    public GooglePlayAppBundleWrapper(IJenkinsBridge jenkins, String path)
    {
        _jenkins = jenkins
    }

    public GooglePlayAppBundleWrapper(IJenkinsBridge jenkins)
    {
        _jenkins = jenkins
    }

    IJenkinsBridge getJenkinsBridge()
    {
        return _jenkins
    }

    ShellCommandBuilder createCommandBuilder()
    {
        return new ShellCommandBuilder()
            .setCommand("bundletool")
    }

    public BuildApksCommand getBuildApksCommand()
    {
        return new BuildApksCommand(this)
    }

    public ExtractUniversalApkCommand getExtractUniversalApkCommand()
    {
        return new ExtractUniversalApkCommand(this)
    }

    public MeasureEstimatedDownloadSizes getMeasureEstimatedDownloadSizesCommand()
    {
        return new MeasureEstimatedDownloadSizes(this)
    }
}
