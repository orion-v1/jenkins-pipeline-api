package io.orion.jenkins.appbundle

import io.orion.jenkins.ShellCommandBuilder

public class MeasureEstimatedDownloadSizes implements Serializable
{
    protected GooglePlayAppBundleWrapper _wrapper
    private BuildApksCommand _buildCommand
    protected Map _args = [:]

    MeasureEstimatedDownloadSizes(GooglePlayAppBundleWrapper wrapper)
    {
        _wrapper = wrapper
    }

    public def setBundlePath(String value)
    {
        if (value != null) _args["--bundle"] = value
        else _args.remove("--bundle")
        return this
    }

    public def setKeystorePath(String value)
    {
        if (value != null) _args["--ks"] = value
        else _args.remove("--ks")
        return this
    }

    public def setKeystorePassword(String value)
    {
        if (value != null) _args["--ks-pass"] = value
        else _args.remove("--ks-pass")
        return this
    }

    public def setKeyAlias(String value)
    {
        if (value != null) _args["--ks-key-alias"] = value
        else _args.remove("--ks-key-alias")
        return this
    }

    public def setKeyPassword(String value)
    {
        if (value != null) _args["--key-pass"] = value
        else _args.remove("--key-pass")
        return this
    }

    public def execute()
    {
        def jenkins = _wrapper.getJenkinsBridge()        
        String tempPath = jenkins.getWorkspaceTempPath()
        String apksPath = "${tempPath}/sizes.apks"

        jenkins.echo("Measure estimated download sizes...")

        // build apks
        ShellCommandBuilder cmd = _wrapper.createCommandBuilder()
            .addOption("build-apks")
            .addOption("--output", apksPath, '=')
            .addOption("--overwrite")
            .addOptions(_args, "=")
        jenkins.sh(cmd.build())

        // estimate sizes
        cmd = _wrapper.createCommandBuilder()
            .addOption("get-size total")
            .addOption("--apks", apksPath, '=')
            .addOption("--human-readable-sizes")
        String stdout = jenkins.sh(true, cmd.build())

        // remove temp file
        jenkins.sh "rm \"${apksPath}\""

        // result
        def rs = parseEstimatedDownloadSizes(stdout)
        if (rs.success) jenkins.echo("Estimated download sizes: min = ${rs.min}, max = ${rs.max}")
        rs
    }

    private def parseEstimatedDownloadSizes(String msg)
    {
        // 'MIN,MAX\n93.64 MB,94.79 MB'
        def rs = [:]
        def found = msg =~ /\n(.*),(.*)$/
        if (found) {
            rs.success = true
            rs.min = found[0][1]
            rs.max = found[0][2]
        }
        else
        {
            rs.success = false
        }

        rs
    }
}
