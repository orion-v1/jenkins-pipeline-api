package io.orion.jenkins.xcode

public class XcodeExportArchiveOptions implements Serializable
{
    private XcodeExportArchiveCommand _command;
    
    private String _method
    private String _teamId
    private boolean _compileBitcode = true
    private boolean _uploadBitcode = true
    private boolean _uploadSymbols = true
    private boolean _manualSigning = false
    private String _signingCertificateName = "iOS Distribution"
    private Map _provisioningProfiles = [:]

    XcodeExportArchiveOptions(XcodeExportArchiveCommand command)
    {
        _command = command
    }

    public XcodeExportArchiveOptions setMethod(String value)
    {
        _method = value
        return this
    }

    public XcodeExportArchiveOptions setAdHocMethod()
    {
        _method = "ad-hoc"
        return this
    }

    public XcodeExportArchiveOptions setAppStoreMethod()
    {
        _method = "app-store"
        return this
    }

    public XcodeExportArchiveOptions setTeamId(String value)
    {
        _teamId = value
        return this
    }

    public XcodeExportArchiveOptions setCompileBitcode(boolean value)
    {
        _compileBitcode = value
        return this
    }

    public XcodeExportArchiveOptions setUploadBitcode(boolean value)
    {
        _uploadBitcode = value
        return this
    }

    public XcodeExportArchiveOptions setUploadSymbols(boolean value)
    {
        _uploadSymbols = value
        return this
    }

    public XcodeExportArchiveOptions setManualSigning(boolean value)
    {
        _manualSigning = value
        return this
    }

    public XcodeExportArchiveOptions setSigningCertificateName(String value)
    {
        _signingCertificateName = value
        return this
    }

    public XcodeExportArchiveOptions addProvisioningProfile(String bundleId, String provisioningProfileAlias)
    {
        _provisioningProfiles[bundleId] = provisioningProfileAlias
        return this
    }

    public XcodeExportArchiveCommand apply()
    {
        return _command
    }

    @Override
    public String toString()
    {
        StringBuilder content = new StringBuilder()

        // add header
        content.append("""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <dict>
        <key>method</key>
        <string>${_method}</string>
""")
        // team id
        if (_teamId != null)
        {
            content.append("        <key>teamID</key>\n")
            content.append("        <string>${_teamId}</string>\n")
        }

        // signing style
        if (!_compileBitcode)
        {
            content.append("        <key>compileBitcode</key>\n")
            content.append("        <false/>\n")
        }

        // allow to upload bitcode
        if (!_uploadBitcode)
        {
            content.append("        <key>uploadBitcode</key>\n")
            content.append("        <false/>\n")
        }

        // allow to upload debug symblos
        if (!_uploadSymbols)
        {
            content.append("        <key>uploadSymbols</key>\n")
            content.append("        <false/>\n")
        }

        // signing style
        if (_manualSigning)
        {
            content.append("        <key>signingStyle</key>\n")
            content.append("        <string>manual</string>\n")

            // signing certificate
            if (_signingCertificateName != null)
            {
                content.append("        <key>signingCertificate</key>\n")
                content.append("        <string>${_signingCertificateName}</string>\n")
            }

            // add provisioning profiles
            if (_provisioningProfiles != null && _provisioningProfiles.size() > 0)
            {
                content.append("        <key>provisioningProfiles</key>\n        <dict>\n")
                _provisioningProfiles.each {
                    content.append("            <key>${it.key}</key>\n")
                    content.append("            <string>${it.value}</string>\n")
                }
                content.append("        </dict>\n")
            }
        }

        // add footer
        content.append("""
    </dict>
</plist>
""")

        return content.toString()
    }
}
