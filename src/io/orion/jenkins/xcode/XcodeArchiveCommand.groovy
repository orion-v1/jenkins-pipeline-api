package io.orion.jenkins.xcode

import io.orion.jenkins.ShellCommandBuilder

// xcodebuild archive \
//      -allowProvisioningUpdates \
//      -derivedDataPath .derivedData \
//      -project "/Users/buildbot/jenkins/workspace/pipeline-test/Builds/jenkins-test-0.1.72-test/Unity-iPhone.xcodeproj" \
//      -scheme "Unity-iPhone" \
//      -archivePath .artifacts/archive.xcarchive \
//      | xcpretty

public class XcodeArchiveCommand extends XcodeCommand
{
    XcodeArchiveCommand(XcodeWrapper xcode)
    {
        super(xcode)
    }

    public XcodeArchiveCommand setAllowProvisioningUpdates(boolean value)
    {
        if (value) _args["-allowProvisioningUpdates"] = null
        else _args.remove("-allowProvisioningUpdates")        
        return this
    }

    public XcodeArchiveCommand setDerivedDataPath(String value)
    {
        if (value != null) _args["-derivedDataPath"] = value
        else _args.remove("-derivedDataPath")
        return this
    }

    public XcodeArchiveCommand setArchivePath(String value)
    {
        if (value != null) _args["-archivePath"] = value
        else _args.remove("-archivePath")
        return this
    }

    public XcodeArchiveCommand setProject(String value)
    {
        if (value != null) _args["-project"] = value
        else _args.remove("-project")
        return this
    }

    public XcodeArchiveCommand setWorkspace(String value)
    {
        if (value != null) _args["-workspace"] = value
        else _args.remove("-workspace")
        return this
    }

    public XcodeArchiveCommand setWorkspaceOrProject(String workspace, String project)
    {
        def jenkins = _xcode.getJenkinsBridge()
        
        if (jenkins.fileExists(workspace)) return setWorkspace(workspace)
        else if (jenkins.fileExists(project)) return setProject(project)
        else jenkins.error("neither project \'${project}\' nor workspace \'${workspace}\' exists.")
    }

    public XcodeArchiveCommand setScheme(String value)
    {
        if (value != null) _args["-scheme"] = value
        else _args.remove("-scheme")
        return this
    }

    public XcodeArchiveCommand setDestination(String value)
    {
        if (value != null) _args["-destination"] = value
        else _args.remove("-destination")
        return this
    }

    public XcodeArchiveCommand setManualSigning(boolean value)
    {
        if (value) _projectOptions["CODE_SIGN_STYLE"] = "Manual"
        else _projectOptions.remove("CODE_SIGN_STYLE")
        return this
    }

    public def setTeamId(String value)
    {
        if (value != null) _projectOptions["DEVELOPMENT_TEAM"] = "${value}"
        else _projectOptions.remove("DEVELOPMENT_TEAM")
        return this
    }

    public def setCodeSignIdentity(String value)
    {
        if (value != null) _projectOptions["CODE_SIGN_IDENTITY"] = "\"${value}\""
        else _projectOptions.remove("CODE_SIGN_IDENTITY")
        return this
    }

    public def setProvisioningProfileSpecifier(String value)
    {
        if (value != null) _projectOptions["PROVISIONING_PROFILE_SPECIFIER"] = "\"${value}\""
        else _projectOptions.remove("PROVISIONING_PROFILE_SPECIFIER")
        return this
    }

    public def setCodeSigningRequired(boolean value)
    {
        _projectOptions["CODE_SIGNING_REQUIRED"] = value ? "YES" : "NO"
        return this        
    }

    public def setCodeSigningAllowed(boolean value)
    {
        _projectOptions["CODE_SIGNING_ALLOWED"] = value ? "YES" : "NO"
        return this        
    }

    public void execute()
    {
        ShellCommandBuilder cmd = createCommandBuilder("xcodebuild", "archive")
        _xcode.appendXcprettyIfAvailable(cmd)

        _xcode.getJenkinsBridge().sh(cmd.build())
    }
}
