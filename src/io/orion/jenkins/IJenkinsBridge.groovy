package io.orion.jenkins

public interface IJenkinsBridge extends Serializable 
{
    String sh(String command)
    String sh(boolean hideStdout, String command)
    String sh(Map args)
    void echo(String message)
    boolean fileExists(String path)
    String readFile(options)
    void writeFile(String path, String text)
    def readJson(options)
    void error(String message)    

    String getWorkspacePath()
    String getWorkspaceTempPath()
}