package io.orion.jenkins.unity.v2

import io.orion.jenkins.IJenkinsBridge
import io.orion.jenkins.ShellCommandBuilder

public class UnityWrapper implements Serializable
{
    private IJenkinsBridge _jenkins
    private String _path

    public UnityWrapper(IJenkinsBridge jenkins, String path)
    {
        _jenkins = jenkins
        _path = path
    }

    IJenkinsBridge getJenkinsBridge()
    {
        return _jenkins
    }

    ShellCommandBuilder createCommandBuilder()
    {
        return new ShellCommandBuilder()
            .setCommand(_path);
    }

    public def getUnityCommand()
    {
        return new UnityCommand(this)
    }

    public def getOrionCommand()
    {
        return new OrionCommand(this)
    }

    public def getBuildProjectCommand()
    {
        return new UnityBuildProjectCommand(this)
    }
}
