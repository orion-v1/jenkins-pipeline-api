package io.orion.jenkins.unity.v2

import io.orion.jenkins.ShellCommandBuilder

public class UnityCommand implements Serializable
{
    protected UnityWrapper _unity
    protected Map _args = [:]

    UnityCommand(UnityWrapper unity)
    {
        _unity = unity
    }

    UnityCommand(UnityWrapper unity, String executeMethod)
    {
        _unity = unity
        //setExecuteMethod(executeMethod) // not supported by jenkins
        if (executeMethod != null) _args["-executeMethod"] = executeMethod
    }

    protected def getJenkinsBridge()
    {
        return _unity.getJenkinsBridge()
    }

    ShellCommandBuilder createCommandBuilder()
    {
        return _unity.createCommandBuilder()
            .addOptions(_args)
    }

    public def setBatchmode(boolean value)
    {
        if (value) _args["-batchmode"] = null
        else _args.remove("-batchmode")        
        return this
    }

    public def setProjectPath(String value)
    {
        if (value != null) _args["-projectPath"] = value
        else _args.remove("-projectPath")
        return this
    }

    public String getProjectPath()
    {
        return _args["-projectPath"]
    }

    public def setQuit(boolean value)
    {
        if (value) _args["-quit"] = null
        else _args.remove("-quit")        
        return this
    }

    public def setAcceptApiUpdate(boolean value)
    {
        if (value) _args["-accept-apiupdate"] = null
        else _args.remove("-accept-apiupdate")        
        return this
    }

    public def setNoGraphics(boolean value)
    {
        if (value) _args["-nographics"] = null
        else _args.remove("-nographics")        
        return this
    }

    public def setBuildTarget(String value)
    {
        if (value != null) _args["-buildTarget"] = value
        else _args.remove("-buildTarget")
        return this
    }

    public String getBuildTarget()
    {
        return _args["-buildTarget"]
    }

    public def setLogFile(String value)
    {
        if (value != null) _args["-logFile"] = value
        else _args.remove("-logFile")
        return this
    }

    public def setExecuteMethod(String value)
    {
        if (value != null) _args["-executeMethod"] = value
        else _args.remove("-executeMethod")
        return this
    }

    public String getExecuteMethod()
    {
        return _args["-executeMethod"]
    }

    public def setUserName(String value)
    {
        if (value != null) _args["-username"] = value
        else _args.remove("-username")
        return this
    }

    public def setPassword(String value)
    {
        if (value != null) _args["-password"] = value
        else _args.remove("-password")
        return this
    }

    public def setSerial(String value)
    {
        if (value != null) _args["-serial"] = value
        else _args.remove("-serial")
        return this
    }

    public def setReturnLicense(boolean value)
    {
        if (value) _args["-returnlicense"] = null
        else _args.remove("-returnlicense")        
        return this
    }

    public def addOption(String option)
    {
        _args[option] = null
        return this
    }

    public def addOption(String option, String value)
    {
        if (value != null) _args[option] = value
        else _args.remove(option)
        return this
    }


    protected def executeCommand(ShellCommandBuilder cmd)
    {
        if (cmd == null) cmd = createCommandBuilder()
        return getJenkinsBridge().sh(cmd.build())
    }

    protected def executeCommand()
    {
        executeCommand(null)
    }

    public Map execute()
    {
        executeCommand(null);
        return [:]
    }
}
