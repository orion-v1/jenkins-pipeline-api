package io.orion.jenkins.unity.v2

import io.orion.jenkins.ShellCommandBuilder

public class UnityBuildProjectCommand extends OrionCommand
{
    UnityBuildProjectCommand(UnityWrapper unity)
    {
        super(unity)
        _args["--build-project"] = null
    }

    public def setProjectName(String value)
    {
        if (value != null) _args["--projectName"] = value
        else _args.remove("--projectName")
        return this
    }

    public def setProjectNameSuffix(String value)
    {
        if (value != null) _args["--projectNameSuffix"] = value
        else _args.remove("--projectNameSuffix")
        return this
    }

    public def setProjectVersion(String value)
    {
        if (value != null) _args["--projectVersion"] = value
        else _args.remove("--projectVersion")
        return this
    }

    public def setProjectVersionSuffix(String value)
    {
        if (value != null) _args["--projectVersionSuffix"] = value
        else _args.remove("--projectVersionSuffix")
        return this
    }

    public def setBuildNumber(int value)
    {
        if (value >= 0) _args["--buildNumber"] = value
        else _args.remove("--buildNumber")
        return this
    }

    public def setBuildsRootPath(String value)
    {
        if (value != null) _args["--buildsRootPath"] = value
        else _args.remove("--buildsRootPath")
        return this
    }

    public def setBuildReportFile(String value)
    {
        if (value != null) _args["--buildReportFile"] = value
        else _args.remove("--buildReportFile")
        return this
    }

    public String getBuildReportFile()
    {
        return _args["--buildReportFile"]
    }

    public def setAndroidExportMode(String value)
    {
        if (value != null) _args["--androidExportMode"] = value
        else _args.remove("--androidExportMode")
        return this
    }

    public def setAppleDeveloperTeamId(String value)
    {
        if (value != null) _args["--appleDeveloperTeamId"] = value
        else _args.remove("--appleDeveloperTeamId")
        return this
    }

    public def setEnableDevelopmentBuild(boolean value)
    {
        if (value) _args["--enableDevelopmentBuild"] = null
        else _args.remove("--enableDevelopmentBuild")
        return this
    }

    public def setEnableScriptDebugging(boolean value)
    {
        if (value) _args["--enableScriptDebugging"] = null
        else _args.remove("--enableScriptDebugging")
        return this
    }

    public def setKeystoreCredentialsFile(String value)
    {
        if (value != null) _args["--keystoreCredentialsFile"] = value
        else _args.remove("--keystoreCredentialsFile")
        return this
    }

    public def setKeystorePath(String value)
    {
        if (value != null) _args["--keystorePath"] = value
        else _args.remove("--keystorePath")
        return this
    }

    public def setKeystorePassword(String value)
    {
        if (value != null) _args["--keystorePassword"] = value
        else _args.remove("--keystorePassword")
        return this
    }

    public def setKeyAlias(String value)
    {
        if (value != null) _args["--keyAlias"] = value
        else _args.remove("--keyAlias")
        return this
    }

    public def setKeyPassword(String value)
    {
        if (value != null) _args["--keyPassword"] = value
        else _args.remove("--keyPassword")
        return this
    }


    @Override
    public Map execute()
    {
        String reportFile = getBuildReportFile();
        if (reportFile == null || reportFile.size() == 0)
        {
            reportFile = "unityProjectBuildReport.json"
            setBuildReportFile(reportFile)
        }

        executeCommand();

        def jenkins = getJenkinsBridge()
        if (!jenkins.fileExists(reportFile))
        {
            jenkins.error("Failed to read Unity Build Report. File '${projectPath}' doesn't exist.")
            return [:]
        }

        return jenkins.readJson(file: reportFile)
    }
}
