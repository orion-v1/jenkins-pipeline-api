package io.orion.jenkins.unity.versioning

import com.cloudbees.groovy.cps.NonCPS

public class UnityVersion implements Serializable, Comparable<UnityVersion>
{
    protected Integer _year = null
    protected Integer _major = null
    protected Integer _minor = null
    protected Character _type = null
    protected Integer _revision = null


    public UnityVersion(int year) 
    {
        if (year < 0) throw new Exception("Unity version year cannot be less than 0.")
        _year = year 
    }
    
    public UnityVersion(int year, int major) 
    {
        this(year)

        if (major < 0) throw new Exception("Unity major version cannot be less than 0.")
        _major = major
    }

    public UnityVersion(int year, int major, int minor) 
    {
        this(year, major)

        if (minor < 0) throw new Exception("Unity minor version cannot be less than 0.")
        _minor = minor
    }

    public UnityVersion(int year, int major, int minor, Character type)
    {
        this(year, major, minor)
        _type = type
    }

    public UnityVersion(int year, int major, int minor, String type)
    {
        this(year, major, minor, type.charAt(0))

        if (type.length() != 1) throw new Exception("Unity version type must contain only one character.")
        _type = type.charAt(0)
    }

    public UnityVersion(int year, int major, int minor, Character type, int revision) 
    {
        this(year, major, minor, type)

        if (revision < 0) throw new Exception("Unity version revision cannot be less than 0.")
        _revision = revision
    }

    public UnityVersion(int year, int major, int minor, String type, int revision) 
    {
        this(year, major, minor, type)

        if (revision < 0) throw new Exception("Unity version revision cannot be less than 0.")
        _revision = revision
    }

    public UnityVersion(String version) 
    {
        def matcher = (version =~ /^(?<year>\d+)(\.(?<major>\d+)(\.(?<minor>\d+)((?<type>[a-z])(?<revision>\d+)?)?)?)?$/)
        
        if (!matcher.find())
            throw new Exception("Invalid Unity version string.")

        def yearGroup = matcher.group("year")
        if (yearGroup) _year = yearGroup.toInteger()

        def majorGroup = matcher.group("major")
        if (majorGroup) _major = majorGroup.toInteger()

        def minorGroup = matcher.group("minor")
        if (minorGroup) _minor = minorGroup.toInteger()

        def typeGroup = matcher.group("type")
        if (typeGroup) _type = typeGroup.charAt(0)

        def revisionGroup = matcher.group("revision")
        if (revisionGroup) _revision = revisionGroup.toInteger()
    }


    @NonCPS
    public Integer getYear() { return _year }
    
    @NonCPS
    public Integer getMajor() { return _major }
    
    @NonCPS
    public Integer getMinor() { return _minor }
    
    @NonCPS
    public Character getType() { return _type }
    
    @NonCPS
    public Integer getRevision() { return _revision }


    @NonCPS
    @Override
    public String toString() 
    {
        StringBuilder builder = new StringBuilder();
        
        if (_year)
            builder.append(_year)

        if (_major)
        {
            builder.append('.')
            builder.append(_major)
        }

        if (_minor)
        {
            builder.append('.')
            builder.append(_minor)
        }

        if (_type)
            builder.append(_type)

        if (_revision)
            builder.append(_revision)

        return builder.toString();
    }


    @NonCPS
    @Override
    public int compareTo(UnityVersion otherVersion)
    {
        if (_year != otherVersion._year) return _year <=> otherVersion._year
        if (_major != otherVersion._major) return _major <=> otherVersion._major
        if (_minor != otherVersion._minor) return _minor <=> otherVersion._minor
        if (_type != otherVersion._type) return _type <=> otherVersion._type
        
        return _revision <=> otherVersion._revision
    }


    @NonCPS
    @Override
    public boolean equals(Object obj)
    {
        if (this.is(obj)) return true

        UnityVersion otherVersion = obj as UnityVersion
        if (!otherVersion) return false

        if (_year != otherVersion._year) return false
        if (_major != otherVersion._major) return false
        if (_minor != otherVersion._minor) return false
        if (_type != otherVersion._type) return false
        
        return _revision == otherVersion._revision
    }
}
