package io.orion.jenkins.unity.versioning.resolvers

import io.orion.jenkins.unity.versioning.UnityVersion

// Fallback chain
public class ChainedUnityVersionResolver implements IUnityVersionResolver
{
    private final List<IUnityVersionResolver> _resolvers


    public ChainedUnityVersionResolver(IUnityVersionResolver... resolvers)
    {
        _resolvers = Arrays.asList(resolvers)
    }


    public UnityVersion resolve(UnityVersion preferredVersion, Iterable<UnityVersion> availableVersions)
    {
        for (int i = 0; i < _resolvers.size(); i++)
        {
            UnityVersion version = _resolvers[i].resolve(preferredVersion, availableVersions)
            if (version != null) return version
        }

        return null
    }
}
