package io.orion.jenkins.unity.versioning.resolvers

import io.orion.jenkins.unity.versioning.UnityVersion

public interface IUnityVersionResolver
{
    public UnityVersion resolve(UnityVersion preferredVersion, Iterable<UnityVersion> availableVersions)
}
