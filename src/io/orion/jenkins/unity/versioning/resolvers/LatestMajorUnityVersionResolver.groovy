package io.orion.jenkins.unity.versioning.resolvers

import io.orion.jenkins.unity.versioning.UnityVersion
import com.cloudbees.groovy.cps.NonCPS

// Searching for the latest available major version of Unity.
// Example:
// (2020.3.23f1, [2019.3.40f1, 2020.3.19f1, 2020.3.42f1, 2021.3.11f1]) -> 2020.3.42f1
// (2020.3, [2019.4.40f1, 2020.3.19f1, 2020.3.42f1, 2021.3.11f1]) -> 2020.3.42f1
public class LatestMajorUnityVersionResolver implements IUnityVersionResolver
{
    @NonCPS
    public UnityVersion resolve(UnityVersion preferredVersion, Iterable<UnityVersion> availableVersions)
    {
        return availableVersions.stream()
            .filter({ version -> 
                if (!preferredVersion.year || preferredVersion.year != version.year) return false
                if (preferredVersion.major && preferredVersion.major != version.major) return false

                return true
            })
            .sorted({ a, b -> b.compareTo(a) }) // reverse order
            .findFirst()
            .orElse(null)
    }
}
